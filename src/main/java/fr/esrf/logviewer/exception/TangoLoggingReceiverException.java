package fr.esrf.logviewer.exception;

/**
 * An {@link Exception} that may be thrown by TangoLoggingReceiver
 * 
 * @author GIRARDOT
 */
public class TangoLoggingReceiverException extends Exception {

    private static final long serialVersionUID = -4611922990151447048L;

    public TangoLoggingReceiverException(String message) {
        super(message);
    }

}
